#!/bin/bash

## docker exec -it cli bash
## docker cp ./scripts/_script.sh cli:/opt/gopath/src/github.com/hyperledger/fabric/peer/scripts/_script.sh


export VAGRANTPATH=/vagrant/less
cp $VAGRANTPATH/docker-compose-e2e-template.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/docker-compose-cli.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/docker-compose-couch.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/peer-base.yaml /home/$(whoami)/fabric/fabric-samples/first-network/base/
cp $VAGRANTPATH/docker-compose-cas-template.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/configtx.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/crypto-config.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/byfn.sh /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/docker-compose-base.yaml /home/$(whoami)/fabric/fabric-samples/first-network/base/
#cp $VAGRANTPATH/script.sh /home/$(whoami)/fabric/fabric-samples/first-network/scripts
cp $VAGRANTPATH/_script.sh /home/$(whoami)/fabric/fabric-samples/first-network/scripts
cp $VAGRANTPATH/byfn-network.json /home/$(whoami)/h29-03b/multi-samples/

rm -rf crypto-config && cryptogen generate --config=./crypto-config.yaml
cp docker-compose-e2e-template.yaml docker-compose-e2e.yaml
cp docker-compose-cas-template.yaml docker-compose-cas.yaml

pushd crypto-config/peerOrganizations/org1.example.com/ca/ && PRIV_KEY=$(ls *_sk) && popd
sed -i "s/CA1_PRIVATE_KEY/${PRIV_KEY}/g" docker-compose-e2e.yaml docker-compose-cas.yaml
pushd crypto-config/peerOrganizations/org2.example.com/ca/ && PRIV_KEY=$(ls *_sk) && popd
sed -i "s/CA2_PRIVATE_KEY/${PRIV_KEY}/g" docker-compose-e2e.yaml docker-compose-cas.yaml

CHANNEL_NAME="mychannel"
configtxgen -profile TwoOrgsOrdererGenesis -outputBlock ./channel-artifacts/genesis.block
configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID $CHANNEL_NAME
configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org1MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org1MSP
configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org2MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org2MSP


#docker rmi `docker images | sed -ne '2,$p' -e 's/  */ /g' | awk '{print $1":"$2}'`
#docker ps -aq | xargs docker stop | xargs docker rm

#CHANNEL_NAME="mychannel" TIMEOUT=10000 DELAY=3 LANG=golang docker-compose -f docker-compose-cli.yaml -f docker-compose-couch.yaml -f docker-compose-cas.yaml up -d 2>&1 && docker logs -f cli
#CHANNEL_NAME="mychannel" TIMEOUT=10000 DELAY=3 LANG=golang docker-compose -f docker-compose-cli.yaml -f docker-compose-cas.yaml up -d 2>&1 && docker logs -f cli
#CHANNEL_NAME="mychannel" TIMEOUT=10000 DELAY=3 LANG=golang docker-compose -f docker-compose-cli.yaml up -d 2>&1 && docker logs -f cli

