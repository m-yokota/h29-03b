# 物理的に隔離されたPCで動作させる。
## メインPC（1台）　←　Restサーバー稼働
## サブPC（複数台）← Peerノードのみを配置

`前提：メインPCに固定IPアドレスを付与する（219.99.44.18）`
上記前提はグローバルIPを利用する場合。
同一ネットワーク上でプライベートIPで行う場合は、192.168.33.100 をkv-serverのアドレスとする。


## メインPC【kv-server】
```
$ cd (repos)/kv-server
$ vagrant up
```

立ち上げたら、下記コマンドでキーバリューサーバーを起動しておく。
```
$ docker run -d -p 8500:8500 -h consul progrium/consul -server -bootstrap
```

このキーバリューサーバーは、ホストPCからポートフォワードでアクセス可能
`v1.vm.network "forwarded_port", guest: 8500, host: 8500`

## メインPC【main】
```
$ cd (repos)/main
$ vagrant up
```

このゲストでRestサーバーを立ち上げるために、3000ポートをフォワードしている。
`v1.vm.network "forwarded_port", guest: 3000, host: 3000`

立ち上げた後、下記コマンドでオーバーレイネットワークを作成する。
```
$ sudo sed -e 's/^ExecStart.*/ExecStart=\/usr\/bin\/dockerd -H fd:\/\/ --cluster-store=consul:\/\/192.168.33.100:8500 --cluster-advertise=192.168.33.111:2376/' /lib/systemd/system/docker.service -i
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
$ docker network create -d overlay overlay_byfn
```

> 同一セグメント内のゲスト同士なので、内部のプライベートIPアドレスで通信可能（192.168.33.100）

Crypt作成のために、下記のスクリプトを実行(Windowsで`git clone`してきた場合、改行コードに注意)
```
$ cd fabric-dev-servers/fabric-samples/first-network
$ /vagrant/generate.sh
$ IMAGE_TAG=latest docker-compose -f docker-compose-cli.yaml -f docker-compose-cas.yaml -f docker-compose-couch.yaml up -d 2>&1
$ docker exec cli scripts/_script.sh
```

## サブPC【sub】
```
$ cd (repos)/sub
$ vagrant up
```

立ち上げた後、下記コマンドでオーバーレイネットワークを作成する。(グローバルIPを利用する場合は、192.168.33.100→219.99.44.18にする)
```
$ sudo sed -e 's/^ExecStart.*/ExecStart=\/usr\/bin\/dockerd -H fd:\/\/ --cluster-store=consul:\/\/192.168.33.100:8500 --cluster-advertise=192.168.33.111:2376/' /lib/systemd/system/docker.service -i
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
```

> `docker network ls` で `overlay_byfn` が存在していることを確認

※　`ホストPCの(repos)/mainフォルダに、メインPCの(repos)/main/crypto-config フォルダを何とかしてコピーしておく`

その後、下記スクリプトを実行
```
$ cd fabric-dev-servers/fabric-samples/first-network
$ /vagrant/sub/add.sh
```

この後、メインPCのcliノードでpeerを参加させる。
```
$ docker exec -it cli bash
```
```
# CORE_PEER_LOCALMSPID="Org2MSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp CORE_PEER_ADDRESS=peer2.org2.example.com:7051 peer channel join -b mychannel.block
```

# Restサーバーの立ち上げ
## メインPC【main】
```
$ /vagrant/rest.sh
$ export PEMPATH=/home/$(whoami)/h29-03b/h29-03b-iothouse
$ composer network start -c PeerAdmin@byfn-network-org1 -n h29-03b-iothouse -V 0.0.2 -A alice -C $PEMPATH/alice/admin-pub.pem -A bob -C $PEMPATH/bob/admin-pub.pem
$ composer card create -p /tmp/composer/org1/byfn-network-org1.json -u alice -n h29-03b-iothouse -c $PEMPATH/alice/admin-pub.pem -k $PEMPATH/alice/admin-priv.pem
$ composer card import -f alice@h29-03b-iothouse.card
$ composer card create -p /tmp/composer/org2/byfn-network-org2.json -u bob -n h29-03b-iothouse -c $PEMPATH/bob/admin-pub.pem -k $PEMPATH/bob/admin-priv.pem
$ composer card import -f bob@h29-03b-iothouse.card
$ composer-rest-server -p 3000 -c bob@h29-03b-iothouse
```

これで、219.99.44.18:3000 にアクセス
（ローカルで実施する場合は192.168.33.100:3000）

# CouchDBからの確認
http://192.168.33.112:11984/_utils/
http://192.168.33.111:5984/_utils/

