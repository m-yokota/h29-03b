#!/bin/bash

echo ""
echo "## サンプルデータ作成 ##"
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{}' 'http://localhost:3000/api/org.itken.SetupDemo'
echo ""
echo "## 契約 ##"
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Agreement", "policyId": "p1", "contractor": "Person002", "company": "ICompany", "iot": "IOT001", "StartDate": "2018-08-10T08:09:28.376Z", "endDate": "2019-08-10T08:09:28.376Z", "address": "TOKYO", "timestamp": "2018-08-10T08:09:28.376Z" }' 'http://localhost:3000/api/org.itken.Agreement'
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Agreement", "policyId": "p2", "contractor": "Person002", "company": "ICompany", "iot": "IOT002", "StartDate": "2018-08-10T08:09:28.376Z", "endDate": "2019-08-10T08:09:28.376Z", "address": "YOKOHAMA", "timestamp": "2018-08-10T08:09:28.376Z" }' 'http://localhost:3000/api/org.itken.Agreement'
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Agreement", "policyId": "p3", "contractor": "Person004", "company": "ICompany", "iot": "IOT003", "StartDate": "2018-08-10T08:09:28.376Z", "endDate": "2019-08-10T08:09:28.376Z", "address": "HIROSHIMA", "timestamp": "2018-08-10T08:09:28.376Z" }' 'http://localhost:3000/api/org.itken.Agreement'
echo ""
echo "## 地震発生 ##"
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Accident", "iotHouse": "IOT002", "accrualDate": "2018-08-10T13:22:59.090Z" }' 'http://localhost:3000/api/org.itken.Accident'
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Accident", "iotHouse": "IOT003", "accrualDate": "2018-08-10T13:22:59.090Z" }' 'http://localhost:3000/api/org.itken.Accident'
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Accident", "iotHouse": "IOT003", "accrualDate": "2018-08-10T13:22:59.090Z" }' 'http://localhost:3000/api/org.itken.Accident'
echo ""
echo "## 被災状況の確認 ##"
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Investigate", "incidentId": "1", "status": "onCorrespond", "incident": "p2-001", "organization": "Org03" }' 'http://localhost:3000/api/org.itken.Investigate'
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Investigate", "incidentId": "2", "status": "onCorrespond", "incident": "p2-001", "organization": "Org05" }' 'http://localhost:3000/api/org.itken.Investigate'
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Investigate", "incidentId": "3", "status": "notCompatible", "incident": "p2-001", "organization": "Org06" }' 'http://localhost:3000/api/org.itken.Investigate'
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Investigate", "incidentId": "4", "status": "onCorrespond", "incident": "p2-001", "organization": "Org01" }' 'http://localhost:3000/api/org.itken.Investigate'

echo ""
echo "## 入札 ##"
#curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.bBidding", "constructionInformation": "CT-p2-001","company": "Company001","bidAmount": 1000000}' 'http://localhost:3000/api/org.itken.bBidding'
#curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Bidding", "constructionInformation": "CT-p2-001","company": "Company002","bidAmount": 1200000}' 'http://localhost:3000/api/org.itken.Bidding'
#curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Bidding", "constructionInformation": "CT-p2-001","company": "Company003","bidAmount": 1300000}' 'http://localhost:3000/api/org.itken.Bidding'
#curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.Bidding", "constructionInformation": "CT-p2-001","company": "Company004","bidAmount": 1400000}' 'http://localhost:3000/api/org.itken.Bidding'

echo ""
echo "## 落札 ##"
#curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.CloseBidding", "information": "CT-p2-001" }' 'http://localhost:3000/api/org.itken.CloseBidding'

echo ""
echo "## フィードバック ##"
#curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "$class": "org.itken.FeedBack", "incident": "p2-001",  "satisfaction": { "$class": "org.itken.CustomerSatisfaction", "score": "1" } }' 'http://localhost:3000/api/org.itken.FeedBack'
