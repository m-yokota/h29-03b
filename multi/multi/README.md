# 諸々のエラーの原因を切り分けたいので、チュートリアル通りのマルチ組織環境を1台の仮想環境にて構築

## Vagrant
```
$ vagrant up
```

## Hyperledger起動
```
cd fabric-dev-servers/fabric-samples/first-network/
/vagrant/generate.sh
IMAGE_TAG=latest docker-compose -f docker-compose-cli.yaml -f docker-compose-cas.yaml -f docker-compose-couch.yaml up -d 2>&1
docker exec cli scripts/_script.sh
```

```
/vagrant/rest.sh
export PEMPATH=/home/$(whoami)/h29-03b/h29-03b-iothouse

```

## Explorer
```
git clone https://github.com/hyperledger/blockchain-explorer.git
cd blockchain-explorer/app/persistence/postgreSQL/
chmod -R 775 db/
chmod db
./createdb.sh
```

cd blockchain-explorer

npm i deep-extend -D
npm i stringstream -D

npm install --save-dev nyc@13.0.1
npm install --save-dev chai-http@4.0.0
npm update istanbul-lib-instrument --depth 2
