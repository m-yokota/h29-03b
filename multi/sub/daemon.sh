#!/bin/bash
#sudo sed -e 's/^ExecStart.*/ExecStart=\/usr\/bin\/dockerd -H fd:\/\/ --cluster-store=consul:\/\/192.168.33.100:8500 --cluster-advertise=192.168.33.112:2376/' /lib/systemd/system/docker.service -i
sudo sed -e 's/^ExecStart.*/ExecStart=\/usr\/bin\/dockerd -H fd:\/\/ --cluster-store=consul:\/\/219.99.44.18:8500 --cluster-advertise=192.168.33.112:2376/' /lib/systemd/system/docker.service -i
sudo systemctl daemon-reload
sudo systemctl restart docker
#docker network ls

export VAGRANTPATH=/vagrant
cp $VAGRANTPATH/docker-compose-e2e-template.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/docker-compose-cli.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/docker-compose-couch.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/peer-base.yaml /home/$(whoami)/fabric/fabric-samples/first-network/base/
cp $VAGRANTPATH/docker-compose-cas-template.yaml /home/$(whoami)/fabric/fabric-samples/first-network/

cp $VAGRANTPATH/configtx.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/crypto-config.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/byfn.sh /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/docker-compose-base.yaml /home/$(whoami)/fabric/fabric-samples/first-network/base/
cp $VAGRANTPATH/script.sh /home/$(whoami)/fabric/fabric-samples/first-network/scripts
