#!/bin/bash

#docker ps -aq | xargs docker rm
#docker rmi `docker images | sed -ne '2,$p' -e 's/  */ /g' | awk '{print $1":"$2}'`


rm -rf /home/$(whoami)/fabric
rm -rf /home/$(whoami)/h29-03b

git clone https://m-yokota@bitbucket.org/m-yokota/h29-03b.git /home/$(whoami)/h29-03b

mkdir /home/$(whoami)/fabric && pushd /home/$(whoami)/fabric
curl -sSL https://goo.gl/kFFqh5 | bash -s 1.1.0
git clone -b issue-6978 https://github.com/sstone1/fabric-samples.git
popd




npm i -g npm
npm install -g composer-cli@0.20.0 && npm install -g composer-rest-server@0.20.0 && npm install -g generator-hyperledger-composer@0.20.0 && npm install -g composer-playground@0.20.0
mkdir ~/fabric-dev-servers && cd ~/fabric-dev-servers
curl -O https://raw.githubusercontent.com/hyperledger/composer-tools/master/packages/fabric-dev-servers/fabric-dev-servers.tar.gz
tar -xvf fabric-dev-servers.tar.gz
export FABRIC_VERSION=hlfv12
./downloadFabric.sh
./startFabric.sh
./createPeerAdminCard.sh

