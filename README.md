# Ubuntu(Vagrantの中でdockerを導入しているけれど、最初からやる場合はdockerも入れなくちゃならないので)
```
sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common
sudo apt autoremove -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get -y install docker-ce
sudo adduser $(whoami) docker
```
ここで一回ログオフする必要がある

## gitのバージョンが多分、2.7.x とかなので、2.9.x以上にこだわる場合（多分、2.7でも良いと思う）
```
sudo add-apt-repository ppa:git-core/ppa
sudo apt-get update -y
sudo apt-get install -y git
```


# Vagrant

## git
```
> git clone https://m-yokota@bitbucket.org/m-yokota/h29-03b.git
> cd h29-03b
```

## Vagrant
```
> vagrant up
> vagrant ssh
```

### 一度構築したけれど、最初からVagrantで構築しなおしたい場合
```
> vagrant status
→running や pause の場合
> vagrant halt
> vagrant destroy -f
> vagrant box list
→ubuntu/xenial64 があることを確認
> vagrant box remove ubuntu/xenial64
```


## 仮想環境上で下記コマンドを順次実行（この内容はVagrantfileに記載したので実行不要）

```
$ sudo apt-get update -y
$ sudo apt-get upgrade -y
$ curl -L https://github.com/docker/compose/releases/download/1.20.1/docker-compose-`uname -s`-`uname -m` > /tmp/docker-compose
$ sudo cp /tmp/docker-compose /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
$ sudo apt-get install -y nodejs npm
$ sudo npm cache clean
$ sudo npm install n -g
$ sudo n 8.9.4
$ sudo apt-get purge -y nodejs npm
$ sudo chown -R $(whoami) $(npm config get prefix)/{lib/node_modules,bin,share}
$ npm install -g yo@2.0.2
$ npm install -g composer-cli@0.19.4
$ npm install -g composer-rest-server@0.19.4
$ npm install -g composer-playground@0.19.4
$ npm install -g generator-hyperledger-composer@0.19.4
```

---

## Hyperledger Fabric 環境を構築

```
$ mkdir ~/fabric
$ cd ~/fabric
$ curl -O https://raw.githubusercontent.com/hyperledger/composer-tools/master/packages/fabric-dev-servers/fabric-dev-servers.tar.gz
$ tar -xvf fabric-dev-servers.tar.gz
$ rm fabric-dev-servers.tar.gz
$ export FABRIC_VERSION=hlfv11
$ ./downloadFabric.sh
$ ./startFabric.sh
$ ./createPeerAdminCard.sh
$ ./stopFabric.sh
```

## マルチ環境Fabricサンプル（todo:byfn.shの内容修正）
```
cd
git clone https://m-yokota@bitbucket.org/m-yokota/h29-03b.git
git clone -b issue-6978 https://github.com/sstone1/fabric-samples.git
cd fabric-samples
##curl -sSL https://goo.gl/6wtTN5 | bash -s 1.1.0	#←いつ変わるかわからんので、現時点での内容をdownload.shに保存しておいた。
cp ~/h29-03b/multi-samples/download.sh ~/fabric-samples/ && chmod +x ~/fabric-samples/download.sh && ~/fabric-samples/download.sh -s 1.1.0
cd ~/fabric-samples/first-network/
./byfn.sh -m generate
./byfn.sh -m up -s couchdb -a
mkdir -p /tmp/composer/org1
mkdir -p /tmp/composer/org2
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt > /tmp/composer/org1/ca-org1.txt && sed -e '1s/^/                "pem":"/g' -e '1s/$/"/g' /tmp/composer/org1/ca-org1.txt -i
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt > /tmp/composer/org2/ca-org2.txt && sed -e '1s/^/                "pem":"/g' -e '1s/$/"/g' /tmp/composer/org2/ca-org2.txt -i
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/ca.crt > /tmp/composer/ca-orderer.txt && sed -e '1s/^/                "pem":"/g' -e '1s/$/"/g' /tmp/composer/ca-orderer.txt -i
cp ~/h29-03b/multi-samples/byfn-network.json /tmp/composer/
sed -e '/INSERT_ORG1_CA_CERT/r /tmp/composer/org1/ca-org1.txt' /tmp/composer/byfn-network.json -e '/INSERT_ORG1_CA_CERT/d' -i
sed -e '/INSERT_ORG2_CA_CERT/r /tmp/composer/org2/ca-org2.txt' /tmp/composer/byfn-network.json -e '/INSERT_ORG2_CA_CERT/d' -i
sed -e '/INSERT_ORDERER_CA_CERT/r /tmp/composer/ca-orderer.txt' /tmp/composer/byfn-network.json -e '/INSERT_ORDERER_CA_CERT/d' -i
cp ~/h29-03b/multi-samples/byfn-network-org1.txt /tmp/composer/org1/
cp ~/h29-03b/multi-samples/byfn-network-org2.txt /tmp/composer/org2/
sed -e '/version/r /tmp/composer/org1/byfn-network-org1.txt' /tmp/composer/byfn-network.json > /tmp/composer/org1/byfn-network-org1.json
sed -e '/version/r /tmp/composer/org2/byfn-network-org2.txt' /tmp/composer/byfn-network.json > /tmp/composer/org2/byfn-network-org2.json
export ORG1=crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
cp -p $ORG1/signcerts/A*.pem /tmp/composer/org1
cp -p $ORG1/keystore/*_sk /tmp/composer/org1
export ORG2=crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
cp -p $ORG2/signcerts/A*.pem /tmp/composer/org2
cp -p $ORG2/keystore/*_sk /tmp/composer/org2
composer card create -p /tmp/composer/org1/byfn-network-org1.json -u PeerAdmin -c /tmp/composer/org1/Admin@org1.example.com-cert.pem -k /tmp/composer/org1/*_sk -r PeerAdmin -r ChannelAdmin -f PeerAdmin@byfn-network-org1.card
composer card create -p /tmp/composer/org2/byfn-network-org2.json -u PeerAdmin -c /tmp/composer/org2/Admin@org2.example.com-cert.pem -k /tmp/composer/org2/*_sk -r PeerAdmin -r ChannelAdmin -f PeerAdmin@byfn-network-org2.card
composer card import -f PeerAdmin@byfn-network-org1.card --card PeerAdmin@byfn-network-org1
composer card import -f PeerAdmin@byfn-network-org2.card --card PeerAdmin@byfn-network-org2
```

```
cd ~/h29-03b/h29-03b-gw02/
composer network install --card PeerAdmin@byfn-network-org1 --archiveFile h29-03b-gw02@0.0.1.bna
composer network install --card PeerAdmin@byfn-network-org2 --archiveFile h29-03b-gw02@0.0.1.bna
composer identity request -c PeerAdmin@byfn-network-org1 -u admin -s adminpw -d alice
composer identity request -c PeerAdmin@byfn-network-org2 -u admin -s adminpw -d bob
cp ~/h29-03b/multi-samples/endorsement-policy.json /tmp/composer/
composer network start -c PeerAdmin@byfn-network-org1 -n h29-03b-gw02 -V 0.0.1 -o endorsementPolicyFile=/tmp/composer/endorsement-policy.json -A alice -C alice/admin-pub.pem -A bob -C bob/admin-pub.pem
composer card create -p /tmp/composer/org1/byfn-network-org1.json -u alice -n h29-03b-gw02 -c alice/admin-pub.pem -k alice/admin-priv.pem
composer card import -f alice@h29-03b-gw02.card
composer network ping -c alice@h29-03b-gw02
composer card create -p /tmp/composer/org2/byfn-network-org2.json -u bob -n h29-03b-gw02 -c bob/admin-pub.pem -k bob/admin-priv.pem
composer card import -f bob@h29-03b-gw02.card
composer network ping -c bob@h29-03b-gw02

```
→この後、composer-rest-server　を起動
```
$ composer-rest-server
? Enter the name of the business network card to use: alice@h29-03b-gw02
? Specify if you want namespaces in the generated REST API: never use namespaces
? Specify if you want to use an API key to secure the REST API: No
? Specify if you want to enable authentication for the REST API using Passport: No
? Specify if you want to enable event publication over WebSockets: Yes
? Specify if you want to enable TLS security for the REST API: No
```

## Hyperledger Composer Playground の起動
```
$ composer-playground
```

## ビジネスネットワークカード（次のBNAファイルを作るためのもの。とりあえず飛ばす。）
```
$ yo hyperledger-composer:businessnetwork
```

---
## BNAファイルのアーカイブ
```
$ cd
$ git clone https://m-yokota@bitbucket.org/m-yokota/h29-03b.git
$ cd ~/h29-03b/h29-03b-gw01
$ composer archive create -t dir -n .
```
上記コマンドで、h29-03b-gw01@0.0.1.bna が作成される。
※package.json の "version" を適宜変えて、バージョンをUpしていく。
※オリジナルでロジック組もうと思ったけれど、とりあえず、サンプルが欲しかったので、一番単純なものをもってきました。


## Fabricへのデプロイ
```
$ cd ~/fabric
$ ./startFabric.sh
$ cd ~/h29-03b/h29-03b-gw01
$ composer network install --card PeerAdmin@hlfv1 --archiveFile h29-03b-gw01@0.0.1.bna
$ composer network start --networkName h29-03b-gw01 --networkVersion 0.0.1 --networkAdmin admin --networkAdminEnrollSecret adminpw --card PeerAdmin@hlfv1 --file networkadmin.card
$ composer card import --file networkadmin.card
$ composer network ping --card admin@h29-03b-gw01
$ composer-rest-server
? Enter the name of the business network card to use: admin@h29-03b-gw01
? Specify if you want namespaces in the generated REST API: never use namespaces
? Specify if you want to enable authentication for the REST API using Passport: No
? Specity if you want to enable event publication over WebSockets: Yes
? Specity if you want to enable TLS security fot the REST API: No
```

`{http://localhost}:3000`

→https://www.evernote.com/shard/s36/sh/af87874c-8068-419e-ad33-662876188fcf/02b6f7cbdcb1aebdd7182b612e7bd839

## RestAPIからの操作

https://www.evernote.com/shard/s36/sh/1930c6fb-f23f-470d-ba89-dd3092028db1/0aeb858757cba14cb32a3f76e7849689
にて公開中（途中）


## 上記APIをWebアプリから叩いてみました。
### httpdをインストール
```
docker pull httpd
```
### index.htmlを用意
h29-03b/httpd/index.html を適当に準備（git に含まれています）←中身で直接192.168.0.7 を指定しているので、各自の環境にあわせて修正

### docker起動
```
docker run -d -p 8080:80 -v "/home/yokota/h29-03b/httpd/:/usr/local/apache2/htdocs/" httpd
```
※/yokota/の部分は各自${whoami}で置き換え

{localhost}:8080 にアクセス

