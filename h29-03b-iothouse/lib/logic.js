/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

/**
 *
 * @param {org.itken.FeedBack} feedBack
 * @transaction
 */
async function toFeedBack(feedBack) {
	console.log('評価');
  // 満足度の登録(1～5の間)
  if (feedBack.satisfaction.score < 1 
      || feedBack.satisfaction.score > 5 ){
    throw new Error('満足度は1～5の間で指定してください。');
  } 

  // インシデントの完了
  feedBack.incident.completionDate = new Date();
  feedBack.incident.satisfaction = feedBack.satisfaction;
  const fr = await getAssetRegistry('org.itken.Incident');
  await fr.update(feedBack.incident);

  // 満足度の登録
  let satisfaction = feedBack.satisfaction;
  feedBack.incident.constInformation.constructionCompany.satisfactions.push(satisfaction);
  const ir = await getParticipantRegistry('org.itken.ConstructionCompany');
  await ir.update(feedBack.incident.constInformation.constructionCompany);  
  
  // IOTのステータスをNormalに戻す
  feedBack.incident.insurancepolicy.iothouse.status = "normal";
  const iot = await getParticipantRegistry('org.itken.IoTHouse');
  await iot.update(feedBack.incident.insurancepolicy.iothouse);
}

/**
 *
 * @param {org.itken.CloseBidding} bidding
 * @transaction
 */
async function CloseBidding(bidding) {
  // 落札じゃー
  console.log('落札じゃ');

  // 工事契約情報に配列で追加していったほうが判断は楽かもしれない。
  
  // 最低入札価格とかの判断もあったほうが良いのかな。
  
  //　ここに、金額だけでなく、満足度とか、契約に応じた判断ロジックが入る
  
  let biddings = [];
  bidding.information.status = 'done';	// ステータスの変更
  //例えば金額の一番安いもの
  let minAmount = 0;
  let minBidding;
  for ( let v of bidding.information.biddings) {
    let sum = 0;
    let cnt = 0;
    // 直近5回の評価値を求める
    v.company.satisfactions.reverse().some(function(value) {
      if (++cnt > 5) { return true; }
      sum += value.score;
    });
	//横田案
    //let valueAmount = v.bidAmount - ((sum == 0 ? 2.9 : sum) / (cnt == 0 ? 1 : cnt) * 0.1 * v.bidAmount);
	//前田案
    let valueAmount = v.bidAmount * (1 - 0.2 * ((sum == 0 ? 2.9 : sum) / (cnt == 0 ? 1 : cnt)));
    if (minAmount == 0 || valueAmount < minAmount) {
      minAmount = valueAmount;
      minBidding = v;
    }
    biddings.push(v);
  }
  // Bid情報を更新
  const br = await getAssetRegistry('org.itken.ConstructionBid');
  await br.updateAll(biddings);
  
  // 落札したBid情報を更新
  console.log(minBidding);
  minBidding.winningBid = true;
  await br.update(minBidding);
  
  bidding.information.winningBid = minBidding;
  bidding.information.constructionCompany = minBidding.company;
  
  // 工事契約情報を更新
  const cr = await getAssetRegistry('org.itken.ConstructionInformation');
  await cr.update(bidding.information);  

  
}
/**
 *
 * @param {org.itken.Bidding} bidding
 * @transaction
 */
async function OnBidding(bidding) {
  // 入札するよ
  console.log('入札じゃ');
  
  // 入札IDは、自動採番(工事契約情報に対して一社が一回しか入札できないのであれば、会社ID＋工事契約IDがそのまま入札IDとなる。)
  const bidId = bidding.company.companyId + '-' + bidding.constructionInformation.constructInformId;
  
  // 入札行為によって、Assetを作成する。
  const registry = await getAssetRegistry('org.itken.ConstructionBid');
  const factory = getFactory();
  const BidAsset = factory.newResource('org.itken'
                                          , 'ConstructionBid', bidId);
  BidAsset.constructionInformation = bidding.constructionInformation;
  BidAsset.company = bidding.company;
  BidAsset.bidAmount = bidding.bidAmount;
  BidAsset.bidDate = new Date();
  
  await registry.add(BidAsset);  
  
  // 工事契約情報に入札情報を追加
  let biddings = bidding.constructionInformation.biddings;
  if (!biddings) {
    biddings = []; 
  }
  biddings.push(BidAsset);
  bidding.constructionInformation.biddings = biddings;
  const ir = await getAssetRegistry('org.itken.ConstructionInformation');
  await ir.update(bidding.constructionInformation);
  
  
  
  
}
/**
 *
 * @param {org.itken.Advertise} advertise
 * @transaction
 */
/*async function toAdvertise(advertise) {
  console.log('保険会社による工事契約情報の掲示処理');
  
  // 工事契約情報の登録
  const registry = await getAssetRegistry('org.itken.ConstructionInformation');
  const factory = getFactory();
  const ConstructionAsset = factory.newResource('org.itken'
                                          , 'ConstructionInformation', advertise.constructInformId);
  ConstructionAsset.incident = advertise.incident;
  await registry.add(ConstructionAsset);  
 
  // ここでインシデントに対して工事契約情報の紐づけをしてあげる。
  const ir = await getAssetRegistry('org.itken.Incident');
  advertise.incident.constInformation = ConstructionAsset;
  await ir.update(advertise.incident); 
  


  
} */

/**
 *
 * @param {org.itken.Investigate} investigate
 * @transaction
 */
async function onInvestigate(investigate) {
  console.log('査定機関による規模見積もり');

  // 保険インシデントを発生させる。
  const registry = await getAssetRegistry('org.itken.InsuranceIncident');
  const factory = getFactory();
  const InvestigateAsset = factory.newResource('org.itken'
                                          , 'InsuranceIncident', investigate.incidentId);
  InvestigateAsset.incident = investigate.incident;
  InvestigateAsset.status = investigate.status;
  InvestigateAsset.organization = investigate.organization;
  await registry.add(InvestigateAsset);

  
  const ir = await getAssetRegistry('org.itken.Incident');
  investigate.incident.incidents.some(function(value) {
    //参照先ではなく、ここに登録されているものだけを変更させてしまう。
    if (value.organization.companyId === investigate.organization.companyId){
      value.enabled = false;  
    }
  });
  investigate.incident.incidents.push(InvestigateAsset);
  await ir.update(investigate.incident); 

  // 改めてonCorrespondの数が一定数以上となったら、advertiseイベントを発生させる。
  let advertiseCount = 0;
  investigate.incident.incidents.some(function(value) {
    if (value.enabled && value.status === 'onCorrespond'){
      advertiseCount++;
    }
  });
  
  if (advertiseCount > 2 && investigate.incident.status === 'notCompatible'){
    // 工事契約情報の登録
    const registry = await getAssetRegistry('org.itken.ConstructionInformation');
    const factory = getFactory();
    const ConstructionAsset = factory.newResource('org.itken'
                                            , 'ConstructionInformation', 'CT-' + investigate.incident.incidentId);
    ConstructionAsset.incident = investigate.incident;
    await registry.add(ConstructionAsset);  

    // ここでインシデントに対して工事契約情報の紐づけをしてあげる。
    const cr = await getAssetRegistry('org.itken.Incident');
    investigate.incident.constInformation = ConstructionAsset;
    await cr.update(investigate.incident); 
    
    investigate.incident.status = 'onCorrespond';
  	await ir.update(investigate.incident); 

  }
  
  
  
}

/**
 *
 * @param {org.itken.Accident} accident
 * @transaction
 */
async function onAccident(accident) {
  console.log('地震発生！地震発生！');
  
  // インシデントの番号は自動取得できないものか。
  // getAll()とかですべての番号の空きとか最大とかを求めるのは、よくよく考えたらデータ量が増えたら現実的でない？
  // 証券番号が一意なので、それにMaxの値を保持しておくとかのほうが良いかも。

  // incidentId
  const incidentId = accident.iotHouse.policy.policyId + '-' + ('00' + (accident.iotHouse.policy.MaxIncident + 1)).slice(-3)
  console.log(incidentId);
  
  const registry = await getAssetRegistry('org.itken.Incident');
  const factory = getFactory();
  const IncidentAsset = factory.newResource('org.itken'
                                          , 'Incident', incidentId);
  IncidentAsset.insurancepolicy = accident.iotHouse.policy;
  IncidentAsset.accrualDate = accident.accrualDate;
  IncidentAsset.incidents = [];
  IncidentAsset.status = 'notCompatible';

  await registry.add(IncidentAsset);
  
  accident.iotHouse.status = 'danger';	// todo: 稼働中...　みたいなステータスになるのかな
  const ir = await getParticipantRegistry('org.itken.IoTHouse');
  await ir.update(accident.iotHouse);
  
  accident.iotHouse.policy.MaxIncident++;
  const ar = await getAssetRegistry('org.itken.InsurancePolicy');
  await ar.update(accident.iotHouse.policy);
  
}

/**
 *
 * @param {org.itken.Agreement} agreement
 * @transaction
 */
async function setAgreement(agreement) {
  // IoTHouseが未割当のものに限る　status="disabled"のもの
  // todo:自動で割り当てる、というやり方の方が良い？どっち？
  if (agreement.iot.status !== 'disabled') {
    throw new Error('指定されたIoTHouse[' + agreement.iot.iotId + ']は既に別の契約に紐づかれています。');
    // ↑この処理って、Web側でチェックすればいいんじゃね？
  }


  // Assetを作成
  const registry = await getAssetRegistry('org.itken.InsurancePolicy');
  const factory = getFactory();

  const PolicyAsset = factory.newResource('org.itken'
                                          , 'InsurancePolicy', agreement.policyId);
  PolicyAsset.iothouse = agreement.iot;
  PolicyAsset.insuranceCompany = agreement.company;
  PolicyAsset.contractor = agreement.contractor;
  PolicyAsset.StartDate = agreement.StartDate;
  PolicyAsset.endDate = agreement.endDate;

  await registry.add(PolicyAsset);

  agreement.iot.status = 'normal';	// todo: 稼働中...　みたいなステータスになるのかな
  agreement.iot.policy = PolicyAsset;
  agreement.iot.address = agreement.address;
  const ir = await getParticipantRegistry('org.itken.IoTHouse');
  await ir.update(agreement.iot);
}

/**
 *
 * @param {org.itken.SetupDemo} setupDemo
 * @transaction
 */
async function setupDemo(setupDemo) {
  const sampleName = ['前田 将彦', '横田 充', '木下 真美子'
                      , '堀切 あすか', '大金 暁', '山口 正志'
                      , '小野間 秀和', '柿沼 文紀', '栗村 彰吾'
                      , '吉原 洸平', '森田 順也']
  let startIndex = 0;
  const factory = getFactory();
  
  // 建設会社
  startIndex = 3;
  const ConstructionCompanys = [
     factory.newResource('org.itken', 'ConstructionCompany', 'Company001')
    ,factory.newResource('org.itken', 'ConstructionCompany', 'Company002')
    ,factory.newResource('org.itken', 'ConstructionCompany', 'Company003')
    ,factory.newResource('org.itken', 'ConstructionCompany', 'Company004')
    ,factory.newResource('org.itken', 'ConstructionCompany', 'Company005')
  ];
  ConstructionCompanys.forEach(function(company, index) {
    company.companyName = sampleName[index + startIndex].split(' ')[0] + '建設';
    company.satisfactions = [];
  });
  const companyRegistry = await getParticipantRegistry('org.itken.ConstructionCompany');
  await companyRegistry.addAll(ConstructionCompanys);
  
  // 保険会社
  startIndex = 0;
  const InsuranceCompanys = [
     factory.newResource('org.itken', 'InsuranceCompany', 'ICompany')
  ];
  InsuranceCompanys.forEach(function(company, index) {
    company.companyName = sampleName[index + startIndex].split(' ')[0] + '保険';
  });
  const InscompanyRegistry = await getParticipantRegistry('org.itken.InsuranceCompany');
  await InscompanyRegistry.addAll(InsuranceCompanys);
  
  // 契約者
  startIndex = 0;
  for (let value of sampleName) {
    const Contractor = factory.newResource('org.itken', 'Contractor'
                                           , 'Person' + ('00' + ++startIndex).slice(-3));
    Contractor.Name = value;
    const ContractorRegistry = await getParticipantRegistry('org.itken.Contractor');
    await ContractorRegistry.add(Contractor);
  }

　// 査定機関
  startIndex = 0;
  for (let value of sampleName) {
    const OrgCompany = factory.newResource('org.itken', 'AssessmentOrganization'
                                           , 'Org' + ('0' + ++startIndex).slice(-2));
    OrgCompany.companyName = value.split(' ')[1] + '調査';
    const ContractorRegistry = await getParticipantRegistry('org.itken.AssessmentOrganization');
    await ContractorRegistry.add(OrgCompany);
  }

  // IoT House  -- 契約によって発生する、という考えではなく、モノ自体は最初から存在していて、契約によって有効化する、という考え。
  let i = 0;
  while( i < 50) {
    const IoTHouse = factory.newResource('org.itken', 'IoTHouse'
                                           , 'IOT' + ('00' + ++i).slice(-3));
    const IoTHouseRegistry = await getParticipantRegistry('org.itken.IoTHouse');
    await IoTHouseRegistry.add(IoTHouse);
  }

}
