## vagrant 立ち上げ
```
vagrant up
vagrant ssh
```

## Hyperledger構築
```
cd ~/fabric-dev-servers
export FABRIC_VERSION=hlfv12
./downloadFabric.sh
./startFabric.sh
./createPeerAdminCard.sh
```

## playground(任意)
```
composer-playground
```
http://192.168.33.111:8080/
(admin, adminpw)

## RestServer
```
pushd /home/$(whoami)/h29-03b/h29-03b-iothouse
git pull
composer archive create -t dir -n .
composer network install --card PeerAdmin@hlfv1 --archiveFile h29-03b-iothouse@0.0.2.bna
composer network start --networkName h29-03b-iothouse --networkVersion 0.0.2 --networkAdmin admin --networkAdminEnrollSecret adminpw --card PeerAdmin@hlfv1 --file networkadmin.card
composer card import --file networkadmin.card
(composer network ping --card admin@h29-03b-iothouse)
popd
composer-rest-server -p 3001 -c admin@h29-03b-iothouse
```
http://192.168.33.111:3001/explorer/

## Destroy
docker ps -aq | xargs docker rm
composer card delete -c admin@marbles-network

## 
=======
## iot house 操作画面
```
cd
git clone https://bitbucket.org/heyheyww1/h29-3b/src/master/
sudo apt-get install -y python3-pip
pip3 install flask
```
起動
```
cd ~/master
python3 main.py
```

## couchdb
`http://192.168.33.111:5984/_utils/`
で、ブロック内容が確認できる。
