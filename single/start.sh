#!/bin/bash

pushd /home/$(whoami)/fabric-dev-servers
MSG = `./startFabric.sh`
echo $MSG
popd

pushd /home/$(whoami)/h29-03b/h29-03b-iothouse
composer archive create -t dir -n .
composer network install --card PeerAdmin@hlfv1 --archiveFile h29-03b-iothouse@0.0.2.bna
composer network start --networkName h29-03b-iothouse --networkVersion 0.0.2 --networkAdmin admin --networkAdminEnrollSecret adminpw --card PeerAdmin@hlfv1 --file networkadmin.card
composer card import --file networkadmin.card
popd
composer-rest-server -p 3001 -c admin@h29-03b-iothouse
echo `http://192.168.33.111:3001/explorer/`

