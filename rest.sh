#!/bin/bash

#docker ps -aq | xargs docker rm
#docker rmi `docker images | sed -ne '2,$p' -e 's/  */ /g' | awk '{print $1":"$2}'`

#composer card delete -c PeerAdmin@byfn-network-org1 && composer card delete -c PeerAdmin@byfn-network-org2 && composer card delete -c PeerAdmin@byfn-network-org9
#composer card delete -c alice@h29-03b-iothouse && composer card delete -c bob@h29-03b-iothouse && composer card delete -c yokota@h29-03b-iothouse

export VAGRANTPATH=/home/$(whoami)/h29-03b/multi-samples
#export VAGRANTPATH=/vagrant/multi-samples

cp $VAGRANTPATH/configtx.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/crypto-config.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/docker-compose-e2e-template.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/docker-compose-cas-template.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/docker-compose-cli.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/docker-compose-couch.yaml /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/byfn.sh /home/$(whoami)/fabric/fabric-samples/first-network/
cp $VAGRANTPATH/docker-compose-base.yaml /home/$(whoami)/fabric/fabric-samples/first-network/base/
cp $VAGRANTPATH/script.sh /home/$(whoami)/fabric/fabric-samples/first-network/scripts

rm -rf /.composer
rm -rf /tmp/composer
mkdir -p /tmp/composer/org1 && mkdir -p /tmp/composer/org2 && mkdir -p /tmp/composer/org9
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt > /tmp/composer/org1/ca-org1.txt && sed -e '1s/^/                "pem":"/g' -e '1s/$/"/g' /tmp/composer/org1/ca-org1.txt -i
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt > /tmp/composer/org2/ca-org2.txt && sed -e '1s/^/                "pem":"/g' -e '1s/$/"/g' /tmp/composer/org2/ca-org2.txt -i
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/peerOrganizations/org9.example.com/peers/peer0.org9.example.com/tls/ca.crt > /tmp/composer/org9/ca-org9.txt && sed -e '1s/^/                "pem":"/g' -e '1s/$/"/g' /tmp/composer/org9/ca-org9.txt -i
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/ca.crt > /tmp/composer/ca-orderer.txt && sed -e '1s/^/                "pem":"/g' -e '1s/$/"/g' /tmp/composer/ca-orderer.txt -i
cp /home/$(whoami)/h29-03b/multi-samples/byfn-network.json /tmp/composer/
sed -e '/INSERT_ORG1_CA_CERT/r /tmp/composer/org1/ca-org1.txt' /tmp/composer/byfn-network.json -e '/INSERT_ORG1_CA_CERT/d' -i
sed -e '/INSERT_ORG2_CA_CERT/r /tmp/composer/org2/ca-org2.txt' /tmp/composer/byfn-network.json -e '/INSERT_ORG2_CA_CERT/d' -i
sed -e '/INSERT_ORG9_CA_CERT/r /tmp/composer/org9/ca-org9.txt' /tmp/composer/byfn-network.json -e '/INSERT_ORG9_CA_CERT/d' -i
sed -e '/INSERT_ORDERER_CA_CERT/r /tmp/composer/ca-orderer.txt' /tmp/composer/byfn-network.json -e '/INSERT_ORDERER_CA_CERT/d' -i
cp /home/$(whoami)/h29-03b/multi-samples/byfn-network-org1.txt /tmp/composer/org1/
cp /home/$(whoami)/h29-03b/multi-samples/byfn-network-org2.txt /tmp/composer/org2/
cp /home/$(whoami)/h29-03b/multi-samples/byfn-network-org9.txt /tmp/composer/org9/
sed -e '/version/r /tmp/composer/org1/byfn-network-org1.txt' /tmp/composer/byfn-network.json > /tmp/composer/org1/byfn-network-org1.json
sed -e '/version/r /tmp/composer/org2/byfn-network-org2.txt' /tmp/composer/byfn-network.json > /tmp/composer/org2/byfn-network-org2.json
sed -e '/version/r /tmp/composer/org9/byfn-network-org9.txt' /tmp/composer/byfn-network.json > /tmp/composer/org9/byfn-network-org9.json
export ORG1=crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
cp -p $ORG1/signcerts/A*.pem /tmp/composer/org1
cp -p $ORG1/keystore/*_sk /tmp/composer/org1
export ORG2=crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
cp -p $ORG2/signcerts/A*.pem /tmp/composer/org2
cp -p $ORG2/keystore/*_sk /tmp/composer/org2
export ORG9=crypto-config/peerOrganizations/org9.example.com/users/Admin@org9.example.com/msp
cp -p $ORG9/signcerts/A*.pem /tmp/composer/org9
cp -p $ORG9/keystore/*_sk /tmp/composer/org9
composer card create -p /tmp/composer/org1/byfn-network-org1.json -u PeerAdmin -c /tmp/composer/org1/Admin@org1.example.com-cert.pem -k /tmp/composer/org1/*_sk -r PeerAdmin -r ChannelAdmin -f PeerAdmin@byfn-network-org1.card
composer card create -p /tmp/composer/org2/byfn-network-org2.json -u PeerAdmin -c /tmp/composer/org2/Admin@org2.example.com-cert.pem -k /tmp/composer/org2/*_sk -r PeerAdmin -r ChannelAdmin -f PeerAdmin@byfn-network-org2.card
composer card create -p /tmp/composer/org9/byfn-network-org9.json -u PeerAdmin -c /tmp/composer/org9/Admin@org9.example.com-cert.pem -k /tmp/composer/org9/*_sk -r PeerAdmin -r ChannelAdmin -f PeerAdmin@byfn-network-org9.card
composer card import -f PeerAdmin@byfn-network-org1.card --card PeerAdmin@byfn-network-org1
composer card import -f PeerAdmin@byfn-network-org2.card --card PeerAdmin@byfn-network-org2
composer card import -f PeerAdmin@byfn-network-org9.card --card PeerAdmin@byfn-network-org9

pushd /home/$(whoami)/h29-03b/h29-03b-iothouse/ && composer archive create -t dir -n .
composer network install --card PeerAdmin@byfn-network-org1 --archiveFile h29-03b-iothouse@0.0.1.bna
composer network install --card PeerAdmin@byfn-network-org2 --archiveFile h29-03b-iothouse@0.0.1.bna
composer network install --card PeerAdmin@byfn-network-org9 --archiveFile h29-03b-iothouse@0.0.1.bna
composer identity request -c PeerAdmin@byfn-network-org1 -u admin -s adminpw -d alice
composer identity request -c PeerAdmin@byfn-network-org2 -u admin -s adminpw -d bob
composer identity request -c PeerAdmin@byfn-network-org9 -u admin -s adminpw -d yokota
popd
cp /home/$(whoami)/h29-03b/multi-samples/endorsement-policy.json /tmp/composer/

#export PEMPATH=/home/$(whoami)/h29-03b/h29-03b-iothouse
#composer network start -c PeerAdmin@byfn-network-org1 -n h29-03b-iothouse -V 0.0.1 -o endorsementPolicyFile=/tmp/composer/endorsement-policy.json -A alice -C $PEMPATH/alice/admin-pub.pem -A bob -C $PEMPATH/bob/admin-pub.pem -A yokota -C $PEMPATH/yokota/admin-pub.pem
#composer card create -p /tmp/composer/org1/byfn-network-org1.json -u alice -n h29-03b-iothouse -c $PEMPATH/alice/admin-pub.pem -k $PEMPATH/alice/admin-priv.pem
#composer card import -f alice@h29-03b-iothouse.card
#composer card create -p /tmp/composer/org2/byfn-network-org2.json -u bob -n h29-03b-iothouse -c $PEMPATH/bob/admin-pub.pem -k $PEMPATH/bob/admin-priv.pem
#composer card import -f bob@h29-03b-iothouse.card
#composer card create -p /tmp/composer/org9/byfn-network-org9.json -u yokota -n h29-03b-iothouse -c $PEMPATH/yokota/admin-pub.pem -k $PEMPATH/yokota/admin-priv.pem
#composer card import -f yokota@h29-03b-iothouse.card


### composer network ping -c bob@h29-03b-iothouse
### docker ps --format "table {{.ID}}\t{{.Names}}\t{{.Status}}\t{{.Command}}\t{{.Ports}}"
#composer-rest-server
#	? Enter the name of the business network card to use: bob@h29-03b-iothouse
#	? Specify if you want namespaces in the generated REST API: never use namespaces
#	? Specify if you want to use an API key to secure the REST API: No
#	? Specify if you want to enable authentication for the REST API using Passport: No
#	? Specify if you want to enable event publication over WebSockets: Yes
#	? Specify if you want to enable TLS security for the REST API: No
