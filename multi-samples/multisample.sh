#!/bin/bash

cd
rm -rf fabric-samples
git clone -b issue-6978 https://github.com/sstone1/fabric-samples.git
cp ~/h29-03b/multi-samples/script.sh ~/fabric-samples/first-network/scripts/
chmod +x ~/fabric-samples/first-network/scripts/script.sh

cp ~/h29-03b/multi-samples/download.sh ~/fabric-samples/ && chmod +x ~/fabric-samples/download.sh
MSG=`~/fabric-samples/download.sh -s 1.1.0`
echo $MSG

cp ~/h29-03b/multi-samples/byfn.sh ~/fabric-samples/first-network/

cd ~/fabric-samples/first-network

#MSG=`./byfn.sh -m down`
#echo $MSG

#MSG=`./byfn.sh -m generate`
#echo $MSG

#MSG=`./byfn.sh -m up -s couchdb -a`
#echo $MSG

