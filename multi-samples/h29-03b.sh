#!/bin/bash

#rm -rf /home/$(whoami)/h29-03b && git clone https://m-yokota@bitbucket.org/m-yokota/h29-03b.git /home/$(whoami)/h29-03b
#rm -rf /home/$(whoami)/fabric-samples && git clone -b issue-6978 https://github.com/sstone1/fabric-samples.git /home/$(whoami)/fabric-samples
#cp ~/h29-03b/multi-samples/download.sh ~/fabric-samples/ && chmod +x ~/fabric-samples/download.sh
#cd ~/fabric-samples && ./download.sh -s 1.1.0



cp ~/h29-03b/multi-samples/docker-compose-cli.yaml ~/fabric-samples/first-network/
cp ~/h29-03b/multi-samples/configtx.yaml ~/fabric-samples/first-network/
cp ~/h29-03b/multi-samples/crypto-config.yaml ~/fabric-samples/first-network/
cp ~/h29-03b/multi-samples/docker-compose-e2e-template.yaml ~/fabric-samples/first-network/
cp ~/h29-03b/multi-samples/docker-compose-base.yaml ~/fabric-samples/first-network/base/
cp ~/h29-03b/multi-samples/byfn.sh ~/fabric-samples/first-network/
cp ~/h29-03b/multi-samples/script.sh ~/fabric-samples/first-network/scripts


