### 環境構築
```
hlf-marbles> vagrant up
hlf-marbles> vagrant ssh
```

### 起動後、下記コマンドを実施
```
cd ~/fabric/marbles/
sudo npm install gulp -g
npm install
rm -rf ~/.hfc-key-store && mkdir ~/.hfc-key-store
cd ~/fabric/fabric-samples/basic-network/
./start.sh
cd ~/fabric/marbles/scripts/
node install_chaincode.js
node instantiate_chaincode.js
```

### URL
cd ~/fabric/marbles/
gulp marbles_local &
`http://192.168.33.99:3001`

### peerコマンド
```
cd ~/fabric
mkdir config; cd config
wget -Nq https://raw.githubusercontent.com/hyperledger/fabric/release-1.0/sampleconfig/core.yaml
```

ピアノードの情報を調べる
docker inspect peer0.org1.example.com | grep -e IPAddress -e MSPID

```
export FABRIC_CFG_PATH=$HOME/fabric/config
export CORE_PEER_ADDRESS=172.21.0.5:7051
export CORE_PEER_LOCALMSPID=Org1MSP
export CORE_PEER_MSPCONFIGPATH=$HOME/fabric/fabric-samples/basic-network/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
```

```
$ peer --help
$ peer version
```

peer chaincode invoke -o localhost:7050 -C mychannel -n marbles -c '{"Args":["init_owner", "o123","Bob","Beedama"]}'


### コマンドでの確認
```
docker exec peer0.org1.example.com peer chaincode query -C mychannel -n marbles -c '{"Args":["read_everything"]}'
```

### 停止
```
fg
^C
cd ../fabric-samples/basic-network/
./stop.sh
docker rm $(docker ps -aq -f 'name=dev-*')
rm -rf ~/.hfc-key-store
./teardown.sh
```

### couchDB
http://192.168.33.99:5984/_utils/

